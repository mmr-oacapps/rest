<?php
namespace pub\helper;

/**
 *
 * @author muhammed.rahman
 *        
 */
class HTTPHandaller {
  
  const cErrorContinue                     = 100;
  const cErrorOK                           = 200;
  const cErrorCreated                      = 201;
  const cErrorAccepted                     = 202;
  const cErrorNonAuthoritativeInformation  = 203;
  const cErrorNoContent                    = 204;
  const cErrorResetContent                 = 205;
  const cErrorPartialContent               = 206;
  const cErrorMultipleChoices              = 300;
  const cErrorMovedPermanently             = 301;
  const cErrorFound                        = 302;
  const cErrorSeeOther                     = 303;
  const cErrorNotModified                  = 304;
  const cErrorUseProxy                     = 305;
  const cErrorTemporaryRedirect            = 307;
  const cErrorBadRequest                   = 400;
  const cErrorUnauthorized                 = 401;
  const cErrorPaymentRequired              = 402;
  const cErrorForbidden                    = 403;
  const cErrorNotFound                     = 404;
  const cErrorMethodNotAllowed             = 405;
  const cErrorNotAcceptable                = 406;
  const cErrroConflict                     = 409;
  const cErrorGone                         = 410;
  const cErrorLengthRequired               = 411;
  const cErrorPreconditionFailed           = 412;
  const cErrorRequestEntityTooLarge        = 413;
  const cErrorRequestURITooLong            = 414;
  const cErrorUnsupportedMediaType         = 415;
  const cErrorRequestedRangeNotSatisfiable = 416;
  const cErrorExpectationFailed            = 417;
  const cErrorInternalServerError          = 500;
  const cErrorNotImplemented               = 501;
  const cErrorServiceUnavailable           = 503;
 
  public function getParams() {

    $replace      = 'url=';
    $replaceWith  = '';
    
		$path   = str_replace( $replace, $replaceWith, preg_replace('/\?.*$/', '', $_SERVER['QUERY_STRING']));
		$params = explode('/', $path);
		
		return $params;
	}
	
	public function getMethod() {
		$method = $_SERVER['REQUEST_METHOD'];
		$override = isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']) ? $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'] : (isset($_GET['method']) ? $_GET['method'] : '');
		if ($method == 'POST' && strtoupper($override) == 'PUT') {
			$method = 'PUT';
		} else if ($method == 'POST' && strtoupper($override) == 'DELETE') {
			$method = 'DELETE';
		} else if ($method == 'POST' && strtoupper($override) == 'PATCH') {
			$method = 'PATCH';
		}
		return $method;
	}
	
	public function getData() {
		$data = file_get_contents('php://input');
		$data = json_decode($data);
		return $data;
	}
	public function httpCodes(){
	  $httpCodes = new \ArrayObject();
	 
	  $httpCodes[self::cContinue]='Continue';
    $httpCodes[self::cErrorOK]='OK';
    $httpCodes[self::cErrorCreated]='Created';
    $httpCodes[self::cErrorAccepted]='Accepted';
    $httpCodes[self::cErrorNonAuthoritativeInformation]='Non-AuthoritativeInformation';
    $httpCodes[self::cErrorNoContent]='NoContent';
    $httpCodes[self::cErrorResetContent]='ResetContent';
    $httpCodes[self::cErrorPartialContent]='PartialContent';
    $httpCodes[self::cErrorMultipleChoices]='MultipleChoices';
    $httpCodes[self::cErrorMovedPermanently]='MovedPermanently';
    $httpCodes[self::cErrorFound]='Found';
    $httpCodes[self::cErrorSeeOther]='SeeOther';
    $httpCodes[self::cErrorNotModified]='NotModified';
    $httpCodes[self::cErrorUseProxy]='UseProxy';
    $httpCodes[self::cErrorTemporaryRedirect]='TemporaryRedirect';
    $httpCodes[self::cErrorBadRequest]='BadRequest';
    $httpCodes[self::cErrorUnauthorized]='Unauthorized';
    $httpCodes[self::cErrorPaymentRequired]='PaymentRequired';
    $httpCodes[self::cErrorForbidden]='Forbidden';
    $httpCodes[self::cErrorNotFound]='NotFound';
    $httpCodes[self::cErrorMethodNotAllowed]='MethodNotAllowed';
    $httpCodes[self::cErrorNotAcceptable]='NotAcceptable';
    $httpCodes[self::cErrroConflict]='Conflict';
    $httpCodes[self::cErrorGone]='Gone';
    $httpCodes[self::cErrorLengthRequired]='LengthRequired';
    $httpCodes[self::cErrorPreconditionFailed]='PreconditionFailed';
    $httpCodes[self::cErrorRequestEntityTooLarge]='RequestEntityTooLarge';
    $httpCodes[self::cErrorRequestURITooLong]='Request-URITooLong';
    $httpCodes[self::cErrorUnsupportedMediaType]='UnsupportedMediaType';
    $httpCodes[self::cErrorRequestedRangeNotSatisfiable]='RequestedRangeNotSatisfiable';
    $httpCodes[self::cErrorExpectationFailed]='ExpectationFailed';
    $httpCodes[self::cErrorInternalServerError]='InternalServerError';
    $httpCodes[self::cErrorNotImplemented]='NotImplemented';
    $httpCodes[self::cErrorServiceUnavailable]='ServiceUnavailable';
	}
	
	return $httpCodes;
	
	
}