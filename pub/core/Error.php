<?php

namespace pub\core;

/**
 *
 * @author muhammed.rahman
 *        
 */
class Error {
  
  private $code;
  private $message;
  
  public function __construct($code, $message){
    $this->code    = $code;
    $this->message = $message;
  }
  
  public final function getCode() {
      return $this->code;
  }
  
  public final function getMessage() {
      return $this->message;
  }

  
  

}

