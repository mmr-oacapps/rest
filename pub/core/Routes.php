<?php

namespace pub\core;

require_once 'helper/HTTPHandaller.php';

use pub\helper\HTTPHandaller as HTTP;

/**
 *
 * @author muhammed.rahman
 *        
 */
class Routes {
  
  private $http;
  
  public function __construct(){
    $this->http = new HTTP();
  }
  
  public function attach($class, $basePath = '') {
  		
  		
  			if (is_string($class) && !class_exists($class)) {
  			  
  			  $error = $this->buildError($this->http::cContinue);
  			  
  			  $this->sendData($error);
  			//	throw new \Exception('Invalid method or class');
  			} else if (!is_string($class) && !is_object($class)) {
  				throw new \Exception('Invalid method or class; must be a classname or object');
  			}
  			if (substr($basePath, 0, 1) == '/') {
  				$basePath = substr($basePath, 1);
  			}
  			if ($basePath && substr($basePath, -1) != '/') {
  				$basePath .= '/';
  			}
  			
  	//	echo $class;
  		
  	}
  	
  public function sendData($data) {    
    var_dump($data);
  		header("Cache-Control: no-cache, must-revalidate");
  		header("Expires: 0");
  		header('Content-Type: application/json' );
  	  echo json_encode($data, 1);  		
  	}  	
  	
  	private function buildError($code){
  	  $error = new \ArrayObject();
  	  $error['code']     = $code;
  	  $error['message']  = (array_key_exists($code, $this->http ->httpCodes)) ?  $this->http->httpCodes[$code] : "Unknown Error";
  	  
  	  return $error;
  	}
  
}

